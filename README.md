# Instalación
 - Requiere Python 3
 - Ejecutar `pip install selenium`
 - Descargar chromedriver, unzip, move to `/usr/local/bin` (mac os / linux) https://chromedriver.chromium.org/downloads
 - Crear las credenciales de Google credentials.json

## Ejecución
Para ejecutar el script:
```
    # Despliegue y validación en sandbox
    time python main.py
```
