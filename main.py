"""
    Script que extrae las transacciones de una cuenta BCP
"""

import os
import platform
import time
import webbrowser

from browsermobproxy import Server
from bs4 import BeautifulSoup
from oauth2client.service_account import ServiceAccountCredentials
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import gspread


class Transaction:
    """
        Clase para generar objetos que registren los campos de la transacción
    """
    date = None
    date_label = 'FECHA'
    sign = None
    sign_label = 'CAUSAL'
    amount = None
    amount_label = 'IMPORTE'
    detail = None
    detail_label = 'GLOSA'
    operation = None
    operation_label = 'OPERACIÓN'

    fields = (
        'date', 'detail', 'sign', 'amount', 'operation',
    )
    MONTHS = (
        'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio',
        'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre',
    )
    def get_month_number(self):
        """
            Retorna el número del mes
        """
        month = self.date.split('/')[1]
        return int(month)

    def get_month_name(self):
        return self.MONTHS[self.get_month_number() - 1]

    def get_row(self):
        row = []
        for field in self.fields:
            row.append(
                getattr(self, field)
            )
        if self.sign == 'CARGO':
            row += ['--', '----']
        return row

    def get_header(self):
        row = []
        for field in self.fields:
            row.append(
                getattr(self, '%s_label' % field)
            )
        return row

class BCPScrap:
    """
        Clase para visitar la web de BCP y extraer las transacciones
    """
    html = None
    soup = None

    def __init__(self):
        print('Inicio...')
        # print('Browsermob...')
        # self.server = Server("%s/browsermob/bin/browsermob-proxy" % os.getcwd())
        # self.server.start()
        # self.proxy = self.server.create_proxy()

        print('Selenium...')
        options = Options()
        options.add_argument('--user-data-dir=.scrap-bcp')  # Mantener sesiones
        # options.add_argument('--headless')  # Levanta chromedriver en background
        options.add_argument('--disable-gpu')  # Necesario temporalmente en windows
        options.add_argument('--log-level=3')  # No muestra en consola log de chromedriver
        options.add_argument('--ignore-certificate-errors')
        # options.add_argument('--proxy-server=%s' % self.proxy.proxy)
        # Levantar el chromedriver
        if platform.system() == "Windows":
            # Ruta del chromedriver de ejemplo, cambiar de no estar en el path
            # De estar el chromedriver en el path solo poner l mismo que en la línea 73
            self.driver = webdriver.Chrome("C:\\ChromeDriver\\chromedriver.exe", options=options)
        else:
            # la ruta del chromedriver debe estar en el path del sistema
            self.driver = webdriver.Chrome(options=options)
        self.driver.implicitly_wait(10)  # Tiempo de espera hasta encontrar un elemento

    def __del__(self):
        print('Fin...')
        self.driver.quit()
        # self.server.stop()

    def login(self):
        print('Login...')
        self.driver.get('data:,')
        self.driver.get('https://bcpzonasegurabeta.viabcp.com')

        while True:
            time.sleep(1)

            if '#/mensaje-de-sesion' in self.driver.current_url:
                self.driver.find_elements_by_tag_name('button')[1].click()
                time.sleep(3)
            
            if '#/portal/mis-productos' in self.driver.current_url:
                break

            if '#/portal/detalle-de-tu-cuenta' in self.driver.current_url:
                break

        try:
            WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable(
                    (By.CLASS_NAME, 'accounts-m-container-uncollapsed')
                )
            ).click()
        except:
            print('warning accounts-m-container-uncollapsed not fonund')

        while '#/portal/detalle-de-tu-cuenta' not in self.driver.current_url:
            time.sleep(1)
    
        return True

    def load_soup(self):
        self.html = self.driver.execute_script(
            "return document.getElementsByTagName('html')[0].innerHTML")
        self.soup = BeautifulSoup(self.html, "html5lib")

    def parse_transactions(self):
        print('parse_transactions...')
        xpath = '//*[@id="wrapper"]/div[2]/nhbk-account-details-component' \
            '/div/div[3]/nhbk-account-summary-card/nhbk-dashboard-wrapper' \
            '/div/div[2]/nhbk-account-summary-table-body/div[{index}]/div/div[4]/div'
        WebDriverWait(self.driver, 30).until(
            EC.element_to_be_clickable(
                (By.XPATH, xpath.format(index=1))
            )
        )
        self.load_soup()
        items = len(self.soup.find_all('div', attrs={'data-index':True}))

        transactions = []
        for index in range(items):
            self.driver.find_element_by_xpath(xpath.format(index=index + 1)).click()
            row = self.wait_elements(self.get_row, index=index)
            transactions.append(row)

        transactions.reverse()
        return transactions

    def get_row(self, index):
        transaction = None
        self.load_soup()
        div = self.soup.find('div', attrs={'data-index':index})

        span = div.find('span', class_='value')
        if span and span.text:
            transaction = Transaction()
            transaction.date = div.find('p', class_='date-td').text.strip()
            tag = div.find('p', class_='amount')

            sign = tag.span.text
            if sign == '+':
                transaction.sign = 'ABONO'
            elif sign == '-':
                transaction.sign = 'CARGO'

            tag.span.extract()
            transaction.amount = tag.text.strip()
            transaction.detail = div.find('p', class_='description').text.strip()
            transaction.operation = span.text

        return transaction

    def wait_elements(self, method, **kwargs):
        for delta in [3, 5, 8]:
            result = method(**kwargs)
            if not result:
                time.sleep(delta)
            else:
                return result

    def get_transactions(self):
        transactions = []
        if self.login():
            transactions = self.parse_transactions()

            if len(transactions) != 20:
                print(transactions)
                print('¡INCOMPLETO!')

        return transactions


class GoogleSpreadsheet:
    """
        Controla las acciones con Google Drive
    """
    file_name = "Operaciones BCP 2020"

    def __init__(self):
        scope = [
            "https://spreadsheets.google.com/feeds",
            'https://www.googleapis.com/auth/spreadsheets',
            "https://www.googleapis.com/auth/drive.file",
            "https://www.googleapis.com/auth/drive"
        ]
        # use creds to create a client to interact with the Google Drive API
        credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scope)
        self.client = gspread.authorize(credentials)
        self.file = self.client.open(self.file_name)

    def load_transactions(self, transactions):
        """
            Carga las nuevas transacciones a Google Drive
        """
        month = None
        sheet = None
        data = []
        news = 0
        for transaction in transactions:
            if month != transaction.get_month_name():
                month = transaction.get_month_name()
                try:
                    sheet = self.file.worksheet(month)
                except gspread.exceptions.WorksheetNotFound:
                    sheet = self.file.add_worksheet(title=month, rows=500, cols=10)
                    row = transaction.fields
                    sheet.append_row(
                        transaction.get_header()
                    )
                data = sheet.get_all_records()

            if self.transaciont_is_new(transaction, data):
                row = transaction.get_row()
                sheet.append_row(row)
                print(row)
                news += 1

        if news == 20:
            print("20 nuevos, validar con el banco")

    def show(self):
        """
            Abre la hoja de calculo en el navegador
        """
        url = "https://docs.google.com/spreadsheets/d/%s" % self.file.id
        webbrowser.open(url, new=2)

    def transaciont_is_new(self, transaction, data):
        """
            Valida que la transacción no esté en la lista de datos
        """
        for obj in data:
            if obj.get(transaction.date_label) == transaction.date \
                and obj.get(transaction.detail_label) == transaction.detail \
                and obj.get(transaction.operation_label) == int(transaction.operation):
                return False
        return True


def main():
    """
        Descarga las transacciones y las guarda en Google Drive
    """
    transactions = BCPScrap().get_transactions()

    spreadsheet = GoogleSpreadsheet()
    spreadsheet.load_transactions(transactions)
    spreadsheet.show()


main()
